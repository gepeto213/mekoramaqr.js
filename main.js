const { readqr, createqr, writeqr, saveqr } = require("./mekoqr");
const { MekoLevel } = require("./mekolevel");

function printBuffer(buffer) {
    let adr = 0;
    function _printAdress() {
        let adress = adr.toString(16).padStart(4, "0");
        process.stdout.write(`${adress}\t`);
    }
    _printAdress();
    for (b of buffer) {
        hexString = b.toString(16).padStart(2, "0");
        if (adr % 16 === 0) {
            process.stdout.write("\n");
            _printAdress();
        }
        process.stdout.write(`${hexString} `);
        adr++;
    }
    process.stdout.write("\n");
}

let imageSrc = __dirname + "/images/test.png";
let imageDst = __dirname + "/images/result.png";
readqr({ imageSrc }) // decoding QR code image
    .then(({ buffer, version }) => {
        let level = new MekoLevel({ buffer, version });
        let { title, author, matrix } = level;

        // Logging level data
        console.log(
            `Title: [${title}] / Author: [${author}] / Version: [${version}]`
        );

        // Test: convert and save to json
        let json = level.toJson();
        level.saveJson(json, `${imageDst}.json`);

        // Test: convert from json
        level.fromJson(json);

        // Rotating
        level.rotate(true);

        // Saving QR code image
        buffer = level.bufferize();
        saveqr({ buffer, version }, imageDst);
    })
    .catch(err => {
        console.log("Error: ", err);
    });
