const express = require("express");
const multer = require("multer");

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

const { readqr, createqr, writeqr, saveqr } = require("./mekoqr");
const { MekoLevel } = require("./mekolevel");

const app = express();
const port = process.env.PORT || 3333;
const host = process.env.HOST || "127.0.0.1";

// If extended is false, you can not post "nested object"
app.use(express.urlencoded({ extended: false, limit: "50mb" }));
app.use(express.json({ limit: "50mb" }));

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});

/**
 * Intended to receive the image data of a level in order o send back the json QR code
 */
app.post("/qrcode/image", upload.single("image"), (req, res, next) => {
    // req.file is the `level` file
    // req.body will hold the text fields, if there were any
    const file = req.file;
    if (file && file.buffer) {
        const { buffer: imageSrc } = file; // this is the file buffer, not the data buffer
        readqr({ imageSrc }) // decoding QR code image
            .then(data => {
                let level = new MekoLevel(data);
                let { title, author, matrix, version } = level;

                // Logging level data
                console.log(
                    `Title: [${title}] / Author: [${author}] / Version: [${version}]`
                );

                // Test: convert and save to json
                const json = level.toJson();
                const encoded_json = Buffer.from(JSON.stringify(json)).toString(
                    "base64"
                );
                const json_data = JSON.stringify({ jsoncode: encoded_json });
                res.send(json_data);
                return;
            })
            .catch(err => {
                const json_data = JSON.stringify({ message: err });
                res.send(json_data);
                return;
            });
    } else {
        res.status(500).send();
    }
});

// const imageDst = __dirname + "/images/result.png";
const imageDst = "base64";

/**
 * Intended to receive the json data of a level in order o send back the base64 QR code
 */
app.post("/qrcode/json", (req, res, next) => {
    const { body } = req;
    const levelData = body["level-data"];
    if (levelData) {
        const hexResponse = body["hex-response"];
        const jsonResponse = body["json-response"];
        const rotation = body["rotation"];

        const jsonData = JSON.parse(levelData);
        const level = new MekoLevel({ jsonData });

        // Managing rotations request
        if (typeof rotation === "number" && rotation > 0) {
            for (let idx = 0; idx < rotation; idx++) {
                level.rotate(true);
            }
        }

        const { version } = level;
        const buffer = level.bufferize();
        saveqr({ buffer, version }, imageDst)
            .then(encoded_qr => {
                const response = { qrdata: encoded_qr };
                if (hexResponse) response["hex"] = buffer.toString("hex");
                if (jsonResponse) response["json"] = level.toJson();
                res.send(response);
            })
            .catch(e => {
                console.log(e);
                res.send({ message: e.message, category: "danger" });
            });
    }
});

app.listen(port, host, () => {
    console.log(`Listening on ${port}...`);
});
