const { MekoBlock, blockTypes } = require("./mekoblocks");

const ndarray = require("ndarray");
const range = n => [...Array(n).keys()];

function codeFlow(code) {
    function* _codeGen(code) {
        let hexData = [...code];
        for (let datum of hexData) {
            yield datum;
        }
    }

    let codeflow = _codeGen(code);
    return () => {
        return codeflow.next().value;
    };
}

class MekoMatrix {
    constructor({ code = undefined, x = 16, y = 16, z = 16 }) {
        this.matrix = ndarray(new Array(x * y * z), [x, y, z]);
        this.shape = this.matrix.shape;
        if (code) this.init(code);
    }

    init(code) {
        let codeflow = codeFlow(code);
        let [mx, my, mz] = this.shape;
        for (let z in range(mz)) {
            for (let y in range(my)) {
                for (let x in range(mx)) {
                    let identifier = codeflow();
                    if (typeof identifier === "undefined") return;

                    let blocktype = blockTypes.values[identifier];
                    if (typeof blocktype === "undefined") {
                        console.log(`ERROR - Unknown blocktype: ${identifier}`);
                        // Unknown blocktype so we attribute the Air one
                        blocktype = blockTypes.names.air;
                    }

                    let block = new MekoBlock(blocktype);
                    let { subtypes } = blocktype;
                    if (subtypes) {
                        block.subtype = codeflow();
                    }

                    this.set(x, y, z, block);
                }
            }
        }
        return this;
    }

    set(x, y, z, value) {
        this.matrix.set(x, y, z, value);
    }

    get(x, y, z) {
        return this.matrix.get(x, y, z);
    }

    rotate(clockwise = false) {
        let [mx, my, mz] = this.shape;
        let m = this.matrix;
        let rot = (x, y, z) => m.get(x, y, z).rotate(clockwise);
        for (let y in range(my)) {
            for (let x in range(mx / 2)) {
                for (let z in range(mz / 2)) {
                    let tobj = rot(x, y, z);
                    if (!clockwise) {
                        m.set(x, y, z, rot(mz - z - 1, y, x));
                        m.set(mz - z - 1, y, x, rot(mx - x - 1, y, mz - z - 1));
                        m.set(mx - x - 1, y, mz - z - 1, rot(z, y, mx - x - 1));
                        m.set(z, y, mx - x - 1, tobj);
                    } else {
                        m.set(x, y, z, rot(z, y, mx - x - 1));
                        m.set(z, y, mx - x - 1, rot(mx - x - 1, y, mz - z - 1));
                        m.set(mx - x - 1, y, mz - z - 1, rot(mz - z - 1, y, x));
                        m.set(mz - z - 1, y, x, tobj);
                    }
                }
            }
        }
    }

    bufferize() {
        let [mx, my, mz] = this.shape;
        let m = this.matrix;
        let a = [];
        for (let z in range(mz)) {
            for (let y in range(my)) {
                for (let x in range(mx)) {
                    let b = m.get(x, y, z);
                    a.push(b.blocktype.value);
                    if (!isNaN(b.subtype)) {
                        a.push(b.subtype);
                    }
                }
            }
        }
        return Buffer.from(a);
    }

    toJson() {
        let blocks = [];
        let [mx, my, mz] = this.shape;
        let m = this.matrix;
        for (let z in range(mz)) {
            for (let y in range(my)) {
                for (let x in range(mx)) {
                    let b = m.get(x, y, z);
                    // console.log(b);
                    let blocktype = b.blocktype;
                    let blockName = blocktype.name.toLowerCase();
                    if (blockName !== "air") {
                        let block = {
                            name: blockName,
                            pos: [parseInt(x), parseInt(y), parseInt(z)]
                        };
                        if (typeof b.subtype !== "undefined") {
                            let subtypes = blocktype.subtypes;
                            let subtype = subtypes[b.subtype];
                            if (subtype) {
                                block.rot = subtype.rot;
                            }
                        }
                        blocks.push(block);
                    }
                }
            }
        }
        return blocks;
    }

    fromJson(blocks) {
        let coord = {};
        blocks.forEach(block => {
            if (block && block.pos) {
                let p = JSON.stringify(block.pos);
                coord[p] = block;
            }
        });
        let coords = Object.keys(coord);

        let [mx, my, mz] = this.shape;
        let m = this.matrix;
        for (let z in range(mz)) {
            for (let y in range(my)) {
                for (let x in range(mx)) {
                    let p = `[${x},${y},${z}]`;
                    let blocktype = undefined,
                        subtype = undefined;

                    if (coords.indexOf(p) > -1) {
                        let block = coord[p];

                        blocktype = blockTypes.names[block.name];
                        if (blocktype.subtypes && block.rot) {
                            subtype = 0;
                            let rot = JSON.stringify(block.rot);
                            let subtypes = blocktype.subtypes;
                            for (const key in subtypes) {
                                let sub = subtypes[key];
                                let subRot = JSON.stringify(sub.rot);
                                if (subRot === rot) {
                                    subtype = key;
                                    break;
                                }
                            }
                        }
                    } else {
                        blocktype = blockTypes.values[0];
                    }

                    let block = new MekoBlock(blocktype, subtype);

                    m.set(x, y, z, block);
                }
            }
        }
    }
}

module.exports = {
    MekoMatrix
};
