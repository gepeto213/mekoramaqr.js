const fs = require("fs");
const path = require("path");

function getBlocksFromDisk() {
    let blocksPath = path.join(__dirname, "blocks");
    let files = fs.readdirSync(blocksPath);
    return files.map(file => {
        let { name, value, subtypes } = getBlockFromDisk(
            path.join(blocksPath, file)
        );
        return {
            name,
            value,
            subtypes
        };
    });
}

function getBlockFromDisk(blockPath) {
    let blockText = fs.readFileSync(blockPath).toString("utf8");
    let jsonText = {};
    try {
        jsonText = JSON.parse(blockText);
    } catch (err) {
        console.error(blockPath, err);
        jsonText = {};
    }
    return jsonText;
}

class MekoBlock {
    constructor(blocktype, subtype = undefined) {
        this.blocktype = blocktype;
        this.subtype = subtype;
    }
    rotate(clockwise = false) {
        let subtype = this.subtype;
        if (!isNaN(subtype)) {
            let { subtypes } = this.blocktype;
            let numRot90 = clockwise ? 3 : 1;
            while (numRot90--) {
                subtype = subtypes[subtype].rot90;
            }
            this.subtype = subtype;
        }
        return this;
    }
}

class BlockType {
    constructor(block) {
        let { name, value, subtypes } = block;
        this.name = name;
        this.value = value;

        if (subtypes) {
            let sb = {};
            for (let [key, subtype] of Object.entries(subtypes)) {
                let { direction, rot, rot90 } = subtype;
                delete subtypes[key];

                subtype = { direction, rot };
                if (typeof rot90 !== "undefined") {
                    subtype.rot90 = parseInt(rot90, 16);
                }

                sb[parseInt(key, 16)] = subtype;
            }
            this.subtypes = sb;
        }
    }
}

class BlockTypes {
    constructor() {
        this.values = {};
        this.names = {};

        let blocks = getBlocksFromDisk();
        blocks.forEach(block => {
            let blockType = new BlockType(block);
            this.values[blockType.value] = blockType;
            this.names[blockType.name.toLowerCase()] = blockType;
        });
    }
}

let blockTypes = new BlockTypes();

module.exports = {
    MekoBlock,
    blockTypes
};
