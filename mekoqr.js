const jsQR = require("jsqr");
const Jimp = require("jimp");
const zlib = require("zlib");
const QRCode = require("qrcode");
const stream = require("stream");
const fs = require("fs");

module.exports = {
    readqr,
    createqr,
    writeqr,
    saveqr
};

const signature = [[1, 2], 19, 13, 252]; // relative to prior and from v1.4
function isSigned(data) {
    try {
        let version = -1;
        let sdata = data.slice(0, 4);
        let index = 0;
        for (const sd of sdata) {
            const sign = signature[index++];
            if (Array.isArray(sign)) {
                version = sign.indexOf(sd);
                if (version < 0) return { valid: false };
            } else if (sign !== sd) {
                return { valid: false };
            }
        }
        return { valid: true, version };
    } catch (error) {}
    return { valid: false };
}

function buildSignature(version = 0) {
    // 0 = prior to v1.4; 1 = from v1.4
    let result = [];
    for (const sign of signature) {
        result.push(Array.isArray(sign) ? sign[version] : sign);
    }
    return result;
}

async function readqr({ imageSrc, base64 = false, signed = true }) {
    return new Promise((resolve, reject) => {
        const getBuffer = data => {
            if (Buffer.isBuffer(data)) {
                return data;
            } else {
                return base64
                    ? Buffer.from(data, "base64")
                    : fs.readFileSync(data);
            }
        };
        let buffer = getBuffer(imageSrc);

        Jimp.read(buffer, function(err, image) {
            if (err) {
                reject(err);
            }

            const code = jsQR(
                image.bitmap.data,
                image.bitmap.width,
                image.bitmap.height
            );
            if (code) {
                let { binaryData, data } = code;

                if (!signed) {
                    resolve(data);
                }

                const { valid, version } = isSigned(binaryData);
                if (!valid) {
                    reject("Bad signature");
                }

                let zlibData = binaryData.slice(-binaryData.length + 4);
                let b = Buffer.from(zlibData);
                zlib.inflate(b, function(err, buffer) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve({ buffer, version });
                    }
                });
            } else {
                reject("QR code not found!");
            }
        });
    });
}

async function createqr({ buffer, version }) {
    return new Promise((resolve, reject) => {
        try {
            let deflated = zlib.deflateSync(buffer, {
                level: zlib.Z_BEST_COMPRESSION
            });

            let binData = [];
            deflated.forEach(elm => binData.push(parseInt(elm)));
            binaryData = buildSignature(version).concat(binData);

            resolve(binaryData);
        } catch (error) {
            reject("Error: ", error);
        }
    });
}

async function writeqr(binaryData, imageDst) {
    return new Promise((resolve, reject) => {
        try {
            let ws = undefined;
            let echoStream = new stream.Writable({
                write: function(chunk, encoding, next) {
                    let buffer = new Buffer.from(chunk, encoding);
                    ws = ws ? Buffer.concat([ws, buffer]) : buffer;
                    next();
                }
            });

            echoStream.on("finish", () => {
                if (imageDst !== "base64") fs.writeFileSync(imageDst, ws);
                let b64 = ws.toString("base64");
                resolve(b64);
            });

            let segs = [{ data: binaryData, mode: "byte" }];
            QRCode.toFileStream(echoStream, segs);
        } catch (error) {
            reject(error);
        }
    });
}

async function saveqr({ buffer, version }, imageDst) {
    return new Promise((resolve, reject) => {
        createqr({ buffer, version }) // re-encoding the same code
            .then(binaryData => {
                writeqr(binaryData, imageDst) // saving to file or only base64 (if imageDst = "base64")
                    .then(b64 => {
                        // b64 is the base64 result for the encoded code
                        resolve(b64);
                    })
                    .catch(error => reject(error));
            })
            .catch(error => reject(error));
    });
}

/*
let imageSrc = __dirname + "/images/mekoqr.png";
let imageDst = __dirname + "/images/result.png";
readqr({ imageSrc }) // decoding QR code image
    .then(({ buffer, version }) => {
        createqr({ buffer, version }) // re-encoding the same code
            .then(binaryData => {
                writeqr(binaryData, imageDst) // saving to file or only base64 (if imageDst = "base64")
                    .then(b64 => {
                        // b64 is the base64 result for the encoded code
                        console.log("Ended");
                    })
                    .catch(error => console.log(error));
            })
            .catch(error => console.log(error));
    })
    .catch(err => {
        console.log("Error: ", err);
    });
console.log("Started");
*/
