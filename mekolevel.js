const fs = require("fs");
const { MekoMatrix } = require("./mekomatrix");

class MekoLevel {
    constructor({ buffer = null, jsonData = null, version = 1 }) {
        this.version = version;
        if (buffer) {
            this.loadCode(buffer);
        } else if (jsonData) {
            this.fromJson(jsonData);
        }
    }

    loadCode(code) {
        let index = 0;
        let size = index + code[index++];
        this.title = code.toString("ascii", index, size + 1);
        index = size + 1;

        size = index + code[index++];
        this.author = code.toString("ascii", index, size + 1);
        index = size + 1;

        let c = code.slice(index);
        this.matrix = new MekoMatrix({ code: c });
    }

    bufferize() {
        let arr = [
            Buffer.from([this.title.length]),
            Buffer.from(this.title, "ascii"),
            Buffer.from([this.author.length]),
            Buffer.from(this.author, "ascii"),
            this.matrix.bufferize()
        ];
        return Buffer.concat(arr);
    }

    toJson() {
        let blocks = this.matrix.toJson();
        return {
            version: this.version,
            data: blocks,
            title: this.title,
            author: this.author
        };
    }

    saveJson(json, jsonFile) {
        json = json || this.toJson();
        fs.writeFile(jsonFile, JSON.stringify(json), "utf8", () => {
            console.log(`JSON Saved to "${jsonFile}"`);
        });
    }

    fromJson(json) {
        let { data, title, author, version } = json;

        this.version = typeof version === "number" ? version : 1;
        this.title = title || "";
        this.author = author || "";

        this.matrix = new MekoMatrix({});
        this.matrix.fromJson(data);
    }

    rotate(clockwise = false) {
        this.matrix.rotate(clockwise);
    }
}

module.exports = {
    MekoLevel
};
